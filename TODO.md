# Cancard

## ToDo

- [ ] Create basic Screen
- [ ] Implement Navigation
- [ ] Implement DI
- [ ] Implement Room
- [ ] Implement Wiki API
- [ ] Implement Repository Pattern for db & API
- [ ] Utils
  - [ ] Datetime calculations
  - [ ] Vibrator (for haptic effects)
  - [ ] Notifications 


     
## In progress

- [ ] Program all composables
- [ ] Download all icons in svg

- [X] Define all functions of each view
  - [X] Define all Composables
  - [ ] Define all models of data (Room)


## Done

- [x] Finalize theme design
  - [X] Colors
  - [x] Font
  - [x] Shapes

- [x] Init android project
  - [x] Add al dependencies 
  - [x] Define min sdk version
 

- [X] Design all views in xd
  - [x] Define flow of application
### Design for views: 

- [x] Home with Events 
- [x] Home without Events 
- [x] Individual Event info
  - [x] Edit Event 
  - [x] Create Event
  - [x] Complete Event
- [x] Event View
  - [x] History Events
    - [x] Individual History Event info
  - [x] Next Events
- [x] Dog data
  - [x] Edit dog data
- [x] Weight Views
  - [x] All Weights View
  - [x] Add new Weight View
  - [x] Individual Weight View
