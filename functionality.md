# Functionality

## Views

### `Home` View

1. Get Dog Data *(from db)*
    1. Dog Name
    1. Convert birth date to months or years
1. Get all Next Events *(from db)*
1. Check if exists Next Events 
    1. Print events
    1. Print TextView and button to create new event
1. Make the top card *(Dog Name & Age)* clickable and navigate to Event View
1. Make the next events card clickable like top card but navigate to individual next event view


---

- Room
- DogData `data class`
- Event `data class`
- Convert date to human readable age
- Navigation
- Dog Card `composable`
- Event Card `composable`
- Background `composable`
- Top App Bar `composable`
- Next Events `composable` with scroll enable
    - LazyColumn


![Home view with next events](/views_designs/01_home_events.png "Home View"){width=40%}

![Home view without next events](/views_designs/02_home_without_events.png "Home View without events"){width=40%}

### Dog Data

#### `Dog Data` View

1. Get Dog Data
    1. Image
    1. Birth date
    1. Breed
    1. Color
    1. Gender
    1. Hair
    1. Chip ID
    1. Birthplace
1. Edit data button (Top App bar)
1. Navigation to edit data
1. Navigation top app bar
1. (Default dog data & user custom data)

![Dog Data View](/views_designs/11_dog_data.png "Dog Data"){width=30%}

#### `Edit Dog Data` View

1. Get Dog Data and place in correct input || empty
    1. Birth date 
        - On Click -> open calendar
    1. Breed
        - List of breeds or other
    1. Color
        - Custom entry
    1. Gender
        - 2 Options on list 
    1. Hair
        - Type of hair, list & custom
    1. Chip ID
        - Custom
    1. Birthplace
        - Map?
    1. Profile image


![Edit Dog Data View](/views_designs/12_edit_dog_data.png "Dog Data"){width=30%}



### Manipulate Events Views

#### `Create Event` View

1. Modal calendar
1. Modal clock
1. Modal map
1. Input text
1. Cancel button navigate previous view (i think Event History or Next Events)
1. Create button, save data in database
    - Enabled when input event name, day/hour & location
    - On create, navigate to next events view

![Create Event View](/views_designs/21_create_event.png "Create event"){width=30%}

#### `Edit Event` View

1. Obtain data from specific event and print into fields
1. If change only data, update in db
1. Show modal dialog to confirm changes
1. Show modal to confirm cancel operation if has changes and click on cancel button

![Edit Event View](/views_designs/22_edit_event.png "Edit event"){width=30%}

#### `Event Info` View

1. Get all data for specific event (from db)
1. Print digital clock with hour of event
1. Print calendar with day of event
1. Print description
1. Enable edit event button
1. Button 'mark as complete' enable navigation to `Complete Event` view

![Event Info view](/views_designs/24_event_info.png "Event Info"){width=30%}


#### `Complete Event` View

1. Get event data (from db)
1. Generate data for new model of data (completed event)
1. Edit event
1. Cancel operation (go to Next Events view)
1. Enable save button when input price
1. Show dialog to confirm complete event
1. Show dialog when if any input field has data & click cancel button
1. Select image from gallery or take photo
1. On save button save in db

![Completed event view](/views_designs/23_event_completed.png "Completed event"){width=30%}


### Events Views


#### `Events` View

1. Show dropdown, whose options:
    - Next events
    - History events

1. Get all next events for Next events option (from dropdown)
    - If exists events, show all events (ListView)
    - If not exists, show message and button to create new event

1. Get all history events for History events option
1. On click one history event go to history event info

![Events view](/views_designs/31_events_view.png ){width=30%}

#### `Events` View dropdown
![Events view dropdown](/views_designs/32_events_dropdown.png ){width=30%}

#### `History Events` view 
![Events view](/views_designs/33_history_events.png ){width=30%}

#### `History Event Info` view

1. Get all information for specific event
1. Enable to delete event
1. If click delete button, show modal dialog to confirm the operation

![History Events Info view](/views_designs/34_history_event_info.png ){width=30%}

### Weights Views

#### `All Weights` view

1. Obtain all weights from db
1. Show last weight on top of view
    - If has image, include the image 
    - If weight has no image, include profile image
1. FAB navigate to add new weight view

![](/views_designs/41_weights.png)

#### `New Weight` view

1. Default date is current day
1. Open calendar when click the date
1. Disable save button for default
    - Enable when input weight & date
1. Show modal dialog if the weight input or photo has modified by user and click on cancel button
1. Save data in db

![](/views_designs/42_new_weight.png)

#### `Weight Info` view

1. Get all information for the specific weight
1. Get dog data
1. Calculate the age for dog with dog data and weight date
1. Enable delete button
    - If click delete button, show modal dialog to confirm or cancel

![](/views_designs/43_weight_info.png)


## Jetpack Compose Components

### Buttons
1. Cancel 
2. FAB
	1. Create event
	2. Add new weight
3. Save 
4. More info 
5. Mark as complete
6. Add Photo
	1. Dog profile
	2. Weight info
	3. Complete event

### Top Bar
1. Drawer (lateral menu)
2. Right button custom on each view
	1. Delete (event, weight)
	2. Edit data (next event, next event when mark as complete, dog data)

### ??
1. Profile image
2. Dog card (Dog name, age, profile pic)
3. Dog card about weight (Text, last weight, profile pic)

1. Digital clock
2. Calendar


4. Disabled Outlined Text Field
	- Dynamic label
	- Dynamic icon
	- Dynamic value



5. Background
6. Content background


## Top App Bar

### Custom part 

In each view the icon on the right is a custom and different functionality.

### Navigation part
1. Home
1. Dog Data
1. Events
1. Weights 


## Notify System

1. WorkManager
