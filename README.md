# Cancard

Android app to track your pet's data. Like clinical data (vaccines, treatments, and more data), name, image, birth date, breed and more.

# Project Mission

Have all my pet's data in a single app, without depending on the notepad, learn more about android development and make personal project.

# Flow app

When I complete all screens designs in Adobe XD I make a basic flow with this views, the flow app was created in [Obsidian](https://obsidian.md/) app and exported to png image.

The first result of how I want the app to work is here:

![](views_designs/flow_canvas.png)



# Requirements

## Design

### Adobe XD

For edit & view the `ui.xd` file.

### Obsidian

For view & interact with the `Flow.canvas` file.

## Development

### Android Studio

Version `Flamingo | 2022.2.1`

### SDK

Min SDK: `29`

### Kotlin

Version `1.8`


# License

GNU General Public License

