# Interesting things for project

## Animations

- ### **`[ART]`** [Animation in Jetpack Compose](https://www.valueof.io/blog/animation-compose-api-summary)

- ### **`[ART]`** [How To Make an Android Run Tracking App](https://www.kodeco.com/28767779-how-to-make-an-android-run-tracking-app) 

- ### **`[ART]`** [Jetpack Compose Animations](https://www.jetpackcompose.net/jetpack-compose-animations) 

- ### **`[YT]`**  [Jetpack Compose Animation](https://www.youtube.com/watch?v=6ZZDPILtYlA) 


## Services

- ### **`[ART]`** [Building an Android service that never stops running](https://robertohuertas.com/2019/06/29/android_foreground_services/)

- ### **`[DOC]`** [ Schedule tasks with WorkManager](https://developer.android.com/topic/libraries/architecture/workmanager)

- ### **`[DOC]`** [WorkManager periodicity](https://medium.com/androiddevelopers/workmanager-periodicity-ff35185ff006)

- ### **`[API]`** [Wikipedia API Example](`https://en.wikipedia.org/w/api.php?action=query&prop=extracts&exsentences=10&exlimit=1&titles=Leishmania&explaintext=1&formatversion=2&format=json&exsectionformat=plain`)
