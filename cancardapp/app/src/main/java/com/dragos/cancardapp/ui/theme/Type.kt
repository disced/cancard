package com.dragos.cancardapp.ui.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import com.dragos.cancardapp.R


private val playfairFamily = FontFamily(
    fonts = listOf(
        Font(
            resId = R.font.playfair_regular,
            weight = FontWeight.Normal,
            style = FontStyle.Normal
        ),
        Font(
            resId = R.font.playfair_semibold,
            weight = FontWeight.SemiBold,
            style = FontStyle.Normal
        ),
        Font(
            resId = R.font.playfair_extrabold,
            weight = FontWeight.ExtraBold,
            style = FontStyle.Normal
        ),
    )
)

val Typography = Typography(playfairFamily)