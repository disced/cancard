package com.dragos.cancardapp.ui.composables

import android.os.VibrationEffect
import android.os.Vibrator
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.dragos.cancardapp.ui.theme.Primary300
import com.dragos.cancardapp.ui.theme.Shapes
import com.dragos.cancardapp.utils.Common
import com.dragos.cancardapp.utils.CustomHaptic

@Composable
fun LargeOutlineButton(
    modifier: Modifier = Modifier,
    onClickButton: () -> Unit,
    text: String,
    haptic: CustomHaptic,
    enabled: Boolean = true
) {

    // TODO: Refactor this functionality to Vibrator.kt util with Dependency Injection for Context.
    val context = LocalContext.current
    val vibrator = context.getSystemService(Vibrator::class.java)
    val oneShot = VibrationEffect.createOneShot(haptic.miliseconds, 1)
    // ---------------------------------------------------------------------------------------------


    OutlinedButton(
        onClick = {
            vibrator.vibrate(oneShot)
            onClickButton()
        },
        enabled = enabled,
        modifier = Modifier
            .padding(
                start = Common.START_END_SPACE,
                end = Common.START_END_SPACE,
                top = 5.dp
            )
            .clip(Shapes.medium)
            .border(
                width = 1.3.dp,
                color = Primary300,
                shape = Shapes.medium
            )
            .height(IntrinsicSize.Min)
            .fillMaxWidth(1f)
            .then(modifier),

        elevation = ButtonDefaults.elevation(
            defaultElevation = 5.dp,
            pressedElevation = 30.dp,
            disabledElevation = 0.dp,
        ),
        content = {
            Text(
                modifier = Modifier
                    .padding(top = 5.dp, bottom = 5.dp),
                fontSize = 19.sp,
                text = text
            )
        }
    )

}