package com.dragos.cancardapp.ui.composables

import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.dragos.cancardapp.ui.theme.Shapes
import com.dragos.cancardapp.utils.Common

@Composable
fun CustomTextField(
    label: String
) {
    var text by rememberSaveable { mutableStateOf("") }

    OutlinedTextField(
        value = text,
        modifier = Modifier
            .padding(
                start = Common.START_END_SPACE,
                end = Common.START_END_SPACE,
                top = 5.dp
            )
            .fillMaxWidth(1f)
            .width(IntrinsicSize.Min),
        shape = Shapes.medium,
        label = { Text(text = label) },
        onValueChange = { text = it },
        singleLine = true,
        keyboardActions = KeyboardActions(onDone = null)
    )
}