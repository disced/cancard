package com.dragos.cancardapp.ui.composables

import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.dragos.cancardapp.ui.theme.Shapes
import com.dragos.cancardapp.utils.Common

@Composable
fun LargeTextInfo(
    text: String,
    label: String
) {
    OutlinedTextField(
        value = text,
        modifier = Modifier
            .padding(
                start = Common.START_END_SPACE,
                end = Common.START_END_SPACE,
                top = 5.dp
            )
            .fillMaxWidth(1f)
            .height(200.dp)
            .width(IntrinsicSize.Min),
        shape = Shapes.medium,
        onValueChange = {},
        label = { Text(text = label) },
        singleLine = false,
        readOnly = true
    )
}