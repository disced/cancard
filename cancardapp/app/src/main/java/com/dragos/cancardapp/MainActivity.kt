package com.dragos.cancardapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Button
import androidx.compose.material.FloatingActionButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.dragos.cancardapp.ui.composables.CustomTextField
import com.dragos.cancardapp.ui.composables.LargeButton
import com.dragos.cancardapp.ui.composables.LargeOutlineButton
import com.dragos.cancardapp.ui.composables.LargeTextInfo
import com.dragos.cancardapp.ui.composables.SmallButton
import com.dragos.cancardapp.ui.composables.SmallOutlineButton
import com.dragos.cancardapp.ui.composables.TextInfoWithIcon
import com.dragos.cancardapp.ui.theme.CancardappTheme
import com.dragos.cancardapp.utils.CustomHaptic

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            CancardappTheme {

                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    TestComposable()
                }
            }
        }
    }
}


@Composable
fun TestComposable() {

    LazyColumn() {
        item {
            Text(
                modifier = Modifier
                    .fillMaxSize(1f),
                textAlign = TextAlign.Center,
                text = "Cancard",
                style = MaterialTheme.typography.h2
            )
        }

        item {
            Button(onClick = { /*TODO*/ }) {
                Text(text = "Boton Prueba")
            }
        }
        item {
            FloatingActionButton(onClick = { /*TODO*/ }) {
                Text(text = "+")
            }
        }
        item {
            CustomTextField(label = "Username")
        }
        item {
            CustomTextField(label = "Password")
        }

        item {
            LargeButton(
                onClickButton = { },
                text = "Large Button",
                haptic = CustomHaptic.SHORT,
            )
        }
        item {
            LargeButton(
                onClickButton = { },
                text = "Another Large Button !! AA",
                haptic = CustomHaptic.LARGE
            )
        }

        item {
            LargeButton(
                onClickButton = { },
                text = "Disabled Button",
                haptic = CustomHaptic.LARGE,
                enabled = false
            )
        }

        item {
            LargeOutlineButton(
                onClickButton = { },
                text = "Outline Large Button",
                haptic = CustomHaptic.MID
            )
        }

//        item {
//            LargeTextField(label = "More description?")
//        }

        item {
            TextInfoWithIcon(
                label = "Date",
                text = "10 de Mayo de 2020",
                icon = painterResource(id = R.drawable.ic_launcher_foreground)
            )
        }

        item {
            TextInfoWithIcon(
                label = "Date",
                text = "10 de Mayo de 2020",
                icon = painterResource(id = R.drawable.ic_launcher_foreground)
            )
        }
        item {
            LargeTextInfo(
                text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut est nisl, tincidunt et blandit a, venenatis quis justo. Mauris odio dui, sodales volutpat porttitor ut, lacinia porta nunc. Pellentesque tempor eleifend ex, quis mattis ligula condimentum vel. Nunc porta ipsum nulla, mollis congue nulla dapibus nec. In hac habitasse platea dictumst. Nam cursus feugiat neque a volutpat. Curabitur non vehicula lorem, nec aliquet ex. Fusce tincidunt sit amet nisl a eleifend. ",
                label = "Description"
            )
        }
        item {
            Row(
                modifier = Modifier
                    .padding(
                        top = 5.dp,
                        start = 20.dp,
                        end = 20.dp
                    )
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
            ) {
                SmallOutlineButton(
                    onClickButton = { },
                    text = "Cancel",
                    haptic = CustomHaptic.MID
                )
                SmallButton(
                    onClickButton = { },
                    text = "Save",
                    haptic = CustomHaptic.MID,
                    enabled = false
                )
            }
        }
    }

}
