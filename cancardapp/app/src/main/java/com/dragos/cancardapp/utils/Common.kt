package com.dragos.cancardapp.utils

import androidx.compose.ui.unit.dp

object Common {
    val START_END_SPACE = 20.dp
    val TOP_SPACE = 10.dp
}