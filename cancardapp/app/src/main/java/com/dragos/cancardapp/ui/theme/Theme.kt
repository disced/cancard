package com.dragos.cancardapp.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.Colors
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable

private fun getDarkColors(): Colors {
    return darkColors(
        primary = Primary200,
        primaryVariant = Primary500,
        secondary = Secondary600,
        secondaryVariant = Secondary700,
        background = Dark,
        surface = Dark2,
        onPrimary = Dark,
        onSecondary = Dark,
        onBackground = Primary200,
        error = Error,
        onError = OnError
    )
}

private fun getLightColors(): Colors {
    return lightColors(
        primary = Primary500,
        primaryVariant = Primary700,
        secondary = Secondary800,
        secondaryVariant = Secondary900,
        onPrimary = Primary50,
        background = Primary50,
        surface = Primary100,
        onSurface = Primary900,
        onBackground = Primary800,
        onSecondary = Secondary50,
        error = Error,
        onError = Primary50
    )
}


@Composable
fun CancardappTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) {
        getDarkColors()
    } else {
        getLightColors()
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}