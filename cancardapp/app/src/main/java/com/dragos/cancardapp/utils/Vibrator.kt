package com.dragos.cancardapp.utils

enum class CustomHaptic(val miliseconds: Long) {
    NONE(0),
    SHORT(45),
    MID(55),
    LARGE(58)
}