package com.dragos.cancardapp.ui.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.unit.dp
import com.dragos.cancardapp.utils.Common

@Composable
fun TextInfoWithIcon(
    text: String,
    label: String,
    icon: Painter
) {
    Column(
        modifier = Modifier
            .padding(
                top = Common.TOP_SPACE,
                start = Common.START_END_SPACE,
                end = Common.START_END_SPACE
            )
            .fillMaxWidth()
    ) {
        Row(verticalAlignment = Alignment.CenterVertically) {

            // Icon
            Image(
                painter = icon,
                contentDescription = "Icon",
                modifier = Modifier
                    .width(48.dp)
                    .padding(end = 5.dp)

            )
            Column {
                // Label
                Text(text = label, style = MaterialTheme.typography.caption)

                // Main text
                Text(text = text, style = MaterialTheme.typography.h6)
            }
        }
        Divider(
            modifier = Modifier
                .padding(top = 5.dp, bottom = 10.dp),
            thickness = 1.dp
        )
    }
}