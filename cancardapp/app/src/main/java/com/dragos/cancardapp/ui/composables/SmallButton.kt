package com.dragos.cancardapp.ui.composables

import android.os.VibrationEffect
import android.os.Vibrator
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.dragos.cancardapp.ui.theme.Shapes
import com.dragos.cancardapp.utils.CustomHaptic


@Composable
fun SmallButton(
    modifier: Modifier = Modifier,
    onClickButton: () -> Unit,
    text: String,
    haptic: CustomHaptic,
    enabled: Boolean = true
) {
    // TODO: Refactor this functionality to Vibrator.kt util with Dependency Injection for Context.
    val context = LocalContext.current
    val vibrator = context.getSystemService(Vibrator::class.java)
    val oneShot = VibrationEffect.createOneShot(haptic.miliseconds, 1)
    // ---------------------------------------------------------------------------------------------

    Button(
        onClick = {
            vibrator.vibrate(oneShot)
            onClickButton()
        },
        enabled = enabled,
        modifier = Modifier
            .width(width = 170.dp)
            .fillMaxWidth()
            .clip(Shapes.medium)
            .then(modifier),
        elevation = ButtonDefaults.elevation(
            defaultElevation = 5.dp,
            pressedElevation = 30.dp,
            disabledElevation = 0.dp,
        ),
        content = {
            Text(
                modifier = Modifier
                    .padding(top = 5.dp, bottom = 5.dp),
                fontSize = 19.sp,
                text = text
            )
        }
    )
}
