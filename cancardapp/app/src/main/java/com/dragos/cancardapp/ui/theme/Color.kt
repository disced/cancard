package com.dragos.cancardapp.ui.theme

import androidx.compose.ui.graphics.Color

val Primary50 = Color(0xFFeee8fc)
val Primary100 = Color(0xFFd4c7f8)
val Primary200 = Color(0xFFb6a2f5)
val Primary300 = Color(0xFF977bf3)
val Primary400 = Color(0xFF7c5def)
val Primary500 = Color(0xFF6040EB) // P
val Primary600 = Color(0xFF533be4)
val Primary700 = Color(0xFF3e33db)
val Primary800 = Color(0xFF262dd5)
val Primary900 = Color(0xFF0020cb)

val Secondary50 = Color(0xFFfefde9)
val Secondary100 = Color(0xFFfef9ca)
val Secondary200 = Color(0xFFfdf5a7)
val Secondary300 = Color(0xFFfcf085)
val Secondary400 = Color(0xFFfaeb6c)
val Secondary500 = Color(0xFFf7e655)
val Secondary600 = Color(0xFFf9d953)
val Secondary700 = Color(0xFFf5c24b)
val Secondary800 = Color(0xFFf1ab43) // P
val Secondary900 = Color(0xFFe98635)

val Error = Color(0xFFCC2900)

val OnError  = Color(0xFFEDEBFF)

val Dark = Color(0xFF121212)
val Dark2 = Color(0x67131313)


